import cv2 as cv
import numpy as np
import xml.etree.ElementTree as ET


"""
Args:
    images_path: path to the directory containing the dataset images;
                 e.g.: "VOCdevkit/VOC_trainval/JPEGImages/".
    labels_path: path to the directory containing the datasets labels;
                 e.g.: "VOCdevkit/VOC_trainval/Annotations/".
    idx_path: path to the file containing the indexes of the images to be loaded;
              e.g.: "ImageSets/Main/train.txt" containing the index of training set images.
    labels_list: list of label names.
                 e.g.: ["aeroplane", "chair", ...]
    ext: string of image extension; e.g.: ".jpg".

Output: 
    data: list of images loaded from the dataset with their respective indexes.
"""
def loadImageDataWithLabels(images_path, labels_path, idx_path, labels_list, ext):
    # File with indexes of train images.
    idx_file = open(idx_path, "r")
    
    # Loads train image indexes into a list.
    idx_list = list()
    for line in idx_file.readlines():
        idx_list.append(line.replace('\n', ''))

    # Loads train images and labels to lists.
    data = list()
    labels = list()
    for idx in idx_list:
        # Adding image to data.
        img = cv.imread(images_path + idx + ext, -1)
        data.append((img))
        # Adding it's labels to labels.
        labels.append(parseImageLabels(labels_path + idx + ".xml", labels_list))

    return data, labels


"""
Function to parse image labels from xml files.

Args:
    xml: xml file with image annotations.
    labels_list: list of label names.
                 e.g.: ["aeroplane", "chair", ...]

Output:
    label: numpy array with image labels telling wether
           an object respective to the label is in the image.
    
This function is currently hard coded to look for object.name.attrib
"""
def parseImageLabels(xml, labels_list):
    root = ET.parse(xml).getroot()
    num_labels = len(labels_list)
    label = np.zeros(num_labels)

    for object in root.iter("object"):
        for idx in range(0, num_labels):
            if object.find("name").text == labels_list[idx]:
                label[idx] = 1

    return label

    