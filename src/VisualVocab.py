import cv2 as cv
import numpy as np
import tensorflow as tf


"""
Returns features vector from train data.
"""
def extractFeatures(train_data):
    detector = cv.xfeatures2d.SIFT_create()
    features = list()
    for image in train_data:
        keypoints, descriptors = detector.detectAndCompute(image, None)
        for row in range(0, descriptors.shape[0]-1):
            features.append(descriptors[row])
    
    features = np.array(features)
    return features
    

"""
Uses k-means clustering to create a visual vocabulary
where the words are the cluster centroids.
"""
def createVisualVocab(features, num_words):
    points = tf.constant(features)
    centroids = tf.Variable(tf.slice(tf.random_shuffle(points), [0, 0], [num_words, 128]))

    points_expanded = tf.expand_dims(points, 0)
    centroids_expanded = tf.expand_dims(centroids, 1)

    distances = tf.reduce_sum(tf.square(tf.subtract(points_expanded, centroids_expanded)), 2)
    assignments = tf.argmin(distances, 0)

    means = []
    for c in range(num_words):
        means.append(tf.reduce_mean(
        tf.gather(points, 
                    tf.reshape(
                    tf.where(
                        tf.equal(assignments, c)
                    ),[1,-1])
                ),reduction_indices=[1]))

    new_centroids = tf.concat(means, 0)
    update_centroids = tf.assign(centroids, new_centroids)

    return centroids